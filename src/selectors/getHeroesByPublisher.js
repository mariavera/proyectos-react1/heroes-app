import { heroes } from "../data/heroes"

export const getHeroesByPublisher = (publisher) => {
    const validPublishers = ['DC Comics', 'Marvel Comics'];
    if (!validPublishers.includes(publisher)) {
        throw new Error(`Publisher "${publisher}" no es correcto`)
    }
const listFilt =  heroes.filter( heroe => heroe.publisher===publisher)
console.log(listFilt)
    return listFilt
}
