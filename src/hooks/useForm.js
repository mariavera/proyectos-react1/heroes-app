import { useState } from "react"


export const useForm = ( initialValue={}) => {
    const [formValue, setformValue] = useState(initialValue)
    const reset = () => {
        setformValue(initialValue);
    }
    
    const handleInputChange = ({target}) => {
        console.log(target.value)
        setformValue({...formValue, [target.name]:target.value})
    }

    return [ formValue, handleInputChange,reset]

}
