import React from 'react'
import { Redirect, Route, Switch } from 'react-router'
import { DcPage } from '../components/dc/DcPage'
import { HeroPage } from '../components/heroes/HeroPage'
import { Marvel } from '../components/marvel/Marvel'
import { SearchScreen } from '../components/search/SearchScreen'
import { Navbar } from '../components/ui/NavBar';
export const DashboardRoutes = () => {
    return (
        <>
            <Navbar />

            <div className="container">
            <Switch>

                <Route exact path="/marvel" component={Marvel} />
                <Route exact path="/hero/:heroeId" component={HeroPage} />
                <Route exact path="/dc" component={DcPage} />
                <Route path="/search" component={SearchScreen} />
                
                <Redirect to='/marvel' />
            </Switch>
            </div>

        </>
    )
}
