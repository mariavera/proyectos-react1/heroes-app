import React from 'react'

export const LoginPage = ({history}) => {
    const handleLogin = () =>{
        history.replace('/')}
    return (
        <div className="container mt-5">
            <h1>LoginPage</h1>
            <hr />
            <button
            className="btn btn-primary"
            onClick={handleLogin}
            >Log In

            </button>
        </div>
    )
}
