import React, { useMemo } from 'react'
import queryString from 'query-string'
import { useLocation } from 'react-router'
import { heroes } from '../../data/heroes'
import { useForm } from '../../hooks/useForm'
import { HeroCard } from '../heroes/HeroCard'
import { getHeroesByName } from '../../selectors/getHeroesByName'

export const SearchScreen = ({ history }) => {
    const location = useLocation();
    const { q = '' } = queryString.parse(location.search);
    const [formValue, handleInputChange, reset] = useForm({
        searchText: q
    }
    )

    const { searchText } = formValue;
    const heroesFiltered =useMemo(() =>  getHeroesByName(q), [q])
    const handleSubmit = (e) => {
        e.preventDefault();
        // reset()
        history.push(`/search?q=${searchText}`)
        console.log(searchText)
    }
    return (
        <div>
            <h1>Search Screen</h1>
            <hr />
            <div className="row">
                <div className="col-5">
                    <h4> Search Form</h4>
                    <hr />
                    <form>
                        <input type='text'
                            placeholder="Find your hero"
                            className="form-control"
                            name='searchText'
                            value={searchText}
                            onChange={handleInputChange} />
                        <button
                            type="submit"
                            onClick={handleSubmit}
                            className="btn m-1 btn-block btn-outline-primary">
                            Search...
              </button>
                    </form>

                </div>
                <div className="col-7">
                    <h4>Results</h4>
                    <hr />
                   { (q==='') && <div className="alert alert-info">
                        Search a hero

                    </div>}

                    { (q!=='' && heroesFiltered.length ===0) && <div className="alert alert-warning">
                        There is no a hero with {q}

                    </div>}
                    {heroesFiltered.map(hero => (
                        <HeroCard key={hero.id} hero={hero} />
                    ))}
                </div>
            </div>
        </div>

    )
}
