import React from 'react'
import { HeroesList } from '../heroes/HeroesList'

export const Marvel = () => {
    return (
        <div>
      <h1>Marvel page</h1>
      <hr/>   
      <HeroesList publisher={'Marvel Comics'}/>   
        </div>
    )
}
