import React from 'react'
import { Link } from 'react-router-dom'

export const HeroCard = ({ hero }) => {
    return (
        <div className="card " style={{ maxWidth: 400 }}>
            <div className="row no-gutters">
                <div className="col-md-12">
                    <img className="card-img-top  animate__animated animate__bounceIn" src={`./assets/heroes/heroes/${hero.id}.jpg`} alt={hero.superhero} style={{ width: '100%' }} />
                    <div className="card-body">
                        <h4 className="card-title">{hero.superhero}</h4>
                        <p className="card-text">{hero.alter_ego}</p>
                        {
                            (hero.alter_ego !== hero.characters)
                            && <p className="card-text">{hero.characters}</p>
                        }
                        <p className="card-text"><small className="text-muted">{hero.first_appearance}</small></p>
                        <div className="d-flex justify-content-between">
                            <div className={`${(hero.publisher === 'Marvel Comics') ? "btn btn-danger" : "btn btn-primary"}  `} >{hero.publisher}</div>
                            <Link to={`./hero/${hero.id}`} className={`${(hero.publisher === 'Marvel Comics') ? "btn btn-outline-danger" : "btn btn-outline-primary"} `} >Ver mas ...</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
