import React, { useMemo } from 'react'
import { Redirect, useParams } from 'react-router'
import { getHeroesById } from '../../selectors/getHeroesById';

export const HeroPage = ({ history }) => {
  const handleBack = () => {
    if (history.length <= 2) {
      history.push('/')
    } else {

      history.goBack();
    }
  }
  const { heroeId } = useParams();
  const hero= useMemo(() => getHeroesById(heroeId), [heroeId])

  console.log(hero)
  if (!hero || hero.length) {
    return <Redirect to="/" />
  }
  return (
    <div className="row mt-5">
      <div className="col-4">
        <img src={`../assets/heroes/heroes/${heroeId}.jpg`} alt={hero.superhero} className='img-thumbnail' />
      </div>
      <div className="col-8">
        <h3>{hero.superhero}</h3>
        <ul className='list-group list-group-flush'>
          <li className="list-group-item"><b> Alter ego</b> {hero.alter_ego}</li>
          <li className="list-group-item"><b> Publisher</b> {hero.publisher}</li>
          <li className="list-group-item"><b> First appearance</b> {hero.first_appearance}</li>
        </ul>
        <h5>Characters</h5>
        <p>{hero.characters}</p>
        <button
          onClick={handleBack}
          className={`${(hero.publisher === 'Marvel Comics') ? 'btn btn-danger' : 'btn btn-primary'} `}>
          Back
</button>

      </div>
    </div>
  )
}
